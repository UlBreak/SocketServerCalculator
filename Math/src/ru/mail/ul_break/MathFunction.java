package ru.mail.ul_break;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MathFunction {

    protected  String performingAndPreparationСalculations(String function){
        function = function.trim();
        function = function.replace(" ", "");
        function = function.replace(",", ".");
        if (!function.startsWith("(") && !function.endsWith(")")) {
            function = addBracketsInStartEnd(function);
        }
        System.out.println(function);
        ArrayList<String> number = new ArrayList<>();
        ArrayList<String> symbol = new ArrayList<>();
        Pattern patternForNumber = Pattern.compile("[1234567890.]+");
        Pattern patternForSymbol = Pattern.compile("[+/ *:-]+");

        function = String.valueOf(clearStr(function));
        function = addBracketsInStartEnd(function);

        Matcher matcherForNumber = patternForNumber.matcher(function);
        Matcher matcherForSymbol = patternForSymbol.matcher(function);

        while (matcherForNumber.find()) {
            number.add(function.substring(matcherForNumber.start(), matcherForNumber.end()));
        }
        while (matcherForSymbol.find()) {
            symbol.add(function.substring(matcherForSymbol.start(), matcherForSymbol.end()));
        }



        if (checkSymbol(symbol) == false) {
            return "Некоркетная последовательность операций";
        }

        return performingСalculations(function);
    }


    protected static String performingСalculations(String str) {
        int amountBracketsOpen = 0, amountBracketsClose = 0;
        String result = null;

        while (!checkingInsideBrackets(str.toCharArray())) {
            ArrayList<Integer> bracketsOpenIndexs = new ArrayList<>(), bracketsCloseIndexs = new ArrayList<>();
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) == '(') {
                    amountBracketsOpen++;
                    bracketsOpenIndexs.add(i);
                }
                if (str.charAt(i) == ')') {
                    amountBracketsClose++;
                    bracketsCloseIndexs.add(i);
                }
            }
            if (amountBracketsOpen != amountBracketsClose) {
                System.err.println("Ошибка в растоновке скобок");
                return "Ошибка";
            }
            char[] strInChar = str.toCharArray();


            if (amountBracketsOpen > 0) {

                char[] check = new char[str.length()];

                int k = 0;
                for (int i = bracketsOpenIndexs.get(amountBracketsOpen - 1) + 1; i <= bracketsCloseIndexs.get(0) - 1; i++) {
                    check[k] = strInChar[i];
                    k++;
                }

                String OriginalStr = String.valueOf(recopyCharDeleteNull(check));

                int indexBracketsOpen, indexBracketsClose, indexOfFun;

                while (!checkingInsideBrackets(check)) {
                    indexBracketsOpen = 0;
                    indexBracketsClose = 0;
                    indexOfFun = 0;

                    for (int i = 0; i < check.length; i++) {
                        if (check[i] == '*' || check[i] == '/' || check[i] == ':' || check[i] == '%') {
                            indexOfFun = i;
                            break;
                        }
                    }

                    if (indexOfFun == 0) {
                        for (int i = 0; i < check.length; i++) {
                            if (check[i] == '+' || check[i] == '-') {
                                indexOfFun = i;
                                break;
                            }
                        }
                    }

                    for (int i = indexOfFun; i >= 0; i--) {
                        if (String.valueOf(check[i]).matches("[1234567890.]")) {
                            indexBracketsOpen = i;
                        } else break;
                    }

                    for (int i = indexOfFun; i < check.length; i++) {
                        if (String.valueOf(check[i]).matches("[1234567890.]")) {
                            indexBracketsClose = i;
                        }
                    }

                    String strArg0 = "";
                    for (int i = indexOfFun - 1; i >= 0 && String.valueOf(check[i]).matches("[1234567890.]"); i--) {
                        if (String.valueOf(check[i]).matches("[1234567890.]")) strArg0 += check[i];
                        else break;
                    }
                    strArg0 = reverse(strArg0);

                    String strArg1 = "";
                    for (int i = indexOfFun + 1; i <= indexBracketsClose && String.valueOf(check[i]).matches("[1234567890.]"); i++) {
                        strArg1 += check[i];
                    }

                    double arg0 = Double.parseDouble(strArg0);
                    double arg1 = Double.parseDouble(strArg1);


                    switch (check[indexOfFun]) {
                        case '+' ->
                                check = String.valueOf(check).replace(strArg0 + "+" + strArg1, String.valueOf(arg0 + arg1)).toCharArray();
                        case '-' ->
                                check = String.valueOf(check).replace(strArg0 + "-" + strArg1, String.valueOf(arg0 - arg1)).toCharArray();
                        case '*' ->
                                check = String.valueOf(check).replace(strArg0 + "*" + strArg1, String.valueOf(arg0 * arg1)).toCharArray();
                        case '/' ->
                                check = String.valueOf(check).replace(strArg0 + "/" + strArg1, String.valueOf(arg0 / arg1)).toCharArray();
                        case ':' ->
                                check = String.valueOf(check).replace(strArg0 + ":" + strArg1, String.valueOf(arg0 / arg1)).toCharArray();
                        case '%' ->
                                check = String.valueOf(check).replace(strArg0 + "%" + strArg1, String.valueOf(arg0 % arg1)).toCharArray();

                    }

                    check = recopyCharDeleteNull(check);

                    if (checkingInsideBrackets(check)) {
                        strInChar = String.valueOf(strInChar).replace("(" + String.valueOf(OriginalStr) + ")", String.valueOf(check)).toCharArray();
                        str  = String.valueOf(strInChar).replace("(" + String.valueOf(OriginalStr) + ")", String.valueOf(check));
                    } else {
                        strInChar = String.valueOf(strInChar).replace(String.valueOf(OriginalStr), String.valueOf(check)).toCharArray();
                        str  = String.valueOf(strInChar).replace(String.valueOf(OriginalStr), String.valueOf(check));
                    }
                    OriginalStr = String.valueOf(check);

                }

                amountBracketsOpen = 0;
                int lastIndex = bracketsOpenIndexs.size();
                bracketsOpenIndexs.remove(lastIndex - 1);
                amountBracketsClose = 0;
                bracketsCloseIndexs.remove(0);


                strInChar = String.valueOf(strInChar).replace("(" + String.valueOf(OriginalStr) + ")", String.valueOf(check)).toCharArray();
                str = String.valueOf(strInChar).replace("(" + String.valueOf(OriginalStr) + ")", String.valueOf(check));


            }
            result = String.valueOf(strInChar);
        }


        return result;

    }

    public static String reverse(String str) {
        return new StringBuilder(str).reverse().toString();
    }

    protected static boolean checkingInsideBrackets(char[] forCheck) {
        for (int i = 0; i < forCheck.length; i++) {
            if (String.valueOf(forCheck[i]).matches("[+/ *:-]")) {
                return false;
            }
        }
        return true;
    }

    protected static String addBracketsInStartEnd(String str) {
        char[] newStr = new char[str.length() + 2];
        newStr[0] = '(';
        newStr[str.length() + 1] = ')';
        for (int i = 0; i < str.length(); i++) {
            newStr[i + 1] = str.charAt(i);
        }
        return String.valueOf(newStr);
    }

    protected static char[] recopyCharDeleteNull(char[] forRecopy) {
        int lastRealIndex = 0;
        for (int i = forRecopy.length - 1; i >= 0; i--) {
            if (forRecopy[i] != '\u0000') {
                lastRealIndex = i;
                break;
            }
        }
        char[] forReturn = new char[lastRealIndex + 1];
        for (int i = 0; i <= lastRealIndex; i++) {
            forReturn[i] = forRecopy[i];
        }
        return forReturn;
    }


    protected static char[] clearStr(String str) {
        int len = str.length();
        int j = 0;
        char[] strInArray = new char[len];
        char[] firstHalf = new char[len];
        char[] secondHalf = new char[len];
        for (int i = 0; i < len; i++) {
            strInArray[i] = str.charAt(i);
        }


        for (int i = 0; i < len; i++) {
            if ((strInArray[i] == '+') && ((strInArray[i + 1] == '+') || (strInArray[i + 1] == ' '))) {
                if (j < len) j = i + 1;
                firstHalf = Arrays.copyOfRange(strInArray, 0, j);
                while (strInArray[j] == '+') {
                    j++;
                }

                secondHalf = Arrays.copyOfRange(strInArray, j, len);
                strInArray = association(firstHalf, secondHalf);
                len = strInArray.length;
            }
            if ((strInArray[i] == '-') && ((strInArray[i + 1] == '-') || (strInArray[i + 1] == ' '))) {
                if (j < len) j = i + 1;
                firstHalf = Arrays.copyOfRange(strInArray, 0, j);
                while (strInArray[j] == '-') {
                    j++;
                }
                secondHalf = Arrays.copyOfRange(strInArray, j, len);
                strInArray = association(firstHalf, secondHalf);
                len = strInArray.length;
            }
            if ((strInArray[i] == ':') && ((strInArray[i + 1] == ':') || (strInArray[i + 1] == ' '))) {
                if (j < len) j = i + 1;
                firstHalf = Arrays.copyOfRange(strInArray, 0, j);
                while (strInArray[j] == ':') {
                    j++;
                }
                secondHalf = Arrays.copyOfRange(strInArray, j, len);
                strInArray = association(firstHalf, secondHalf);
                len = strInArray.length;
            }
            if ((strInArray[i] == '/') && ((strInArray[i + 1] == '/') || (strInArray[i + 1] == ' '))) {
                if (j < len) j = i + 1;
                firstHalf = Arrays.copyOfRange(strInArray, 0, j);
                while (strInArray[j] == '/') {
                    j++;
                }
                secondHalf = Arrays.copyOfRange(strInArray, j, len);
                strInArray = association(firstHalf, secondHalf);
                len = strInArray.length;
            }
            if ((strInArray[i] == '.') && ((strInArray[i + 1] == '.') || (strInArray[i + 1] == ' '))) {
                if (j < len) j = i + 1;
                firstHalf = Arrays.copyOfRange(strInArray, 0, j);
                while (strInArray[j] == '.') {
                    j++;
                }
                secondHalf = Arrays.copyOfRange(strInArray, j, len);
                strInArray = association(firstHalf, secondHalf);
                len = strInArray.length;
            }
            if ((strInArray[i] == '*') && ((strInArray[i + 1] == '*') || (strInArray[i + 1] == ' '))) {
                if (j < len) j = i + 1;
                firstHalf = Arrays.copyOfRange(strInArray, 0, j);
                while (strInArray[j] == '*') {
                    j++;
                }
                secondHalf = Arrays.copyOfRange(strInArray, j, len);
                strInArray = association(firstHalf, secondHalf);
                len = strInArray.length;
            }
        }


        return strInArray;
    }


    private static char[] association(char[] first, char[] second) {
        int len1 = first.length;
        int len2 = second.length;
        int len3 = len1 + len2;
        char[] newChar = new char[len3];
        int i = 0;
        for (; i < len1; i++) {
            newChar[i] = first[i];
        }
        for (int j = 0; j < len2; j++) {
            newChar[i] = second[j];
            i++;
        }

        return newChar;
    }

    private static Boolean checkSymbol(ArrayList<String> symbol) {
        int size = symbol.size();
        boolean statusForContinue = true;
        for (int i = 0; i < size; i++) {
            char[] aym = symbol.get(i).toCharArray();
            if (aym.length > 1) {
                System.err.println(String.valueOf(aym) + " - Некоркетная последовательность операций");
                statusForContinue = false;
            }
        }
        return statusForContinue;
    }


}





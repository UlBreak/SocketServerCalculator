package ru.mail.ul_break;

import ru.mail.ul_break.MathFunction;
import ru.mail.ul_break.Phone;


import java.io.*;
import java.util.Scanner;


public class Client {
    public static void main(String arg[]){

        Scanner scanner = new Scanner(System.in);


        try (Phone phone = new Phone("169.254.84.58",8000)) {
            System.out.println("Connected to server");
            System.out.print("Enter formula:");
            String request = scanner.nextLine();
            System.out.println("Request: " + request);
            phone.writeLine(request);
            String response = phone.readLine();
            System.out.println("Response: " + response);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}

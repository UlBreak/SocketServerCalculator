package ru.mail.ul_break;

import ru.mail.ul_break.Phone;
import ru.mail.ul_break.MathFunction;

import java.io.*;
import java.net.ServerSocket;

public class Server {
    public static void main(String args[]) {

        MathFunction mathFunction = new MathFunction();
        try (ServerSocket serverSocket = new ServerSocket(8000))
        {
            System.out.println("ru.mail.ul_break.Server started");
            while (true) {
                Phone phone = new Phone(serverSocket);


                new Thread(() -> {
                    String request = phone.readLine();
                    System.out.println("Request: " + request);


                    String response = mathFunction.performingAndPreparationСalculations(request);
                    System.out.println(response);

                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }

                    phone.writeLine(response);
                    System.out.println("Response: "+response);
                    try {
                        phone.close();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }).start();





            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }




    }
}

